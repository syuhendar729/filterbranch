
# filterbranch
Hapus tracking suatu file dari sejarah git


Jika Anda ingin menghapus sejarah (tracking history) suatu file di Git sehingga file tersebut tidak terdeteksi dari commit awal sampai commit akhir, Anda dapat menggunakan perintah filter-branch untuk mengubah sejarah repositori dan menghapus sejarah yang terkait dengan file tersebut. Namun, perlu diingat bahwa perintah filter-branch adalah perintah yang kuat dan berbahaya, karena dapat mengubah sejarah repositori secara permanen. Pastikan untuk melakukan cadangan (backup) repositori Anda sebelum menjalankan perintah ini.

Berikut langkah-langkah untuk menghapus sejarah suatu file dari commit awal sampai commit akhir:

Langkah 1: Cadangkan Repositori
Sebelum melakukan perubahan, buatlah salinan cadangan (backup) dari repositori Anda untuk menghindari kehilangan data jika terjadi kesalahan. Cadangkan seluruh direktori repositori Anda.

Langkah 2: Buat File .gitignore
Pastikan file yang ingin Anda hapus dari sejarah commit tidak tercantum di dalam file .gitignore. Jika file tersebut masih ada di dalam .gitignore, pastikan untuk menghapusnya dari daftar tersebut agar file tersebut dapat terdeteksi.

Langkah 3: Jalankan Perintah filter-branch
Untuk menghapus sejarah file dari commit awal sampai commit akhir, gunakan perintah filter-branch dengan argumen --index-filter untuk menghapus jejak dari indeks Git. Gantilah <file_path> dengan path file yang ingin Anda hapus dari sejarah.

```bash
git filter-branch --index-filter 'git rm --cached --ignore-unmatch <file_path>' -- --all
```

Perintah di atas akan menghapus file dari indeks di setiap commit yang ada di semua cabang (branch) dalam repositori Anda.

Langkah 4: Force Push
Setelah menjalankan perintah filter-branch, repositori lokal Anda telah diubah. Untuk mengirimkan perubahan ini ke repositori remote, Anda perlu menggunakan perintah force push:

```bash
git push --force --all
```

Pastikan untuk melakukan force push dengan hati-hati, karena ini akan mengganti sejarah di repositori remote dan dapat berdampak pada rekan kerja yang berkolaborasi dengan Anda pada repositori ini.

Ingat bahwa perintah filter-branch hanya perlu dijalankan satu kali. Setelah menjalankannya, sejarah file yang dihapus akan terhapus secara permanen dari repositori Anda. Seiring waktu, Anda akan melihat bahwa file tersebut tidak terdeteksi dari commit awal sampai commit akhir.



